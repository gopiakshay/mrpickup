var prodimg = [],
  addprod_images = [];
$("#pickuppn").focusout(function() {
  $('#deliverypn').val($("#pickuppn").val());
});


$(function() {

  if (window.File && window.FileList && window.FileReader) {
    // addproduct images change fn
    $("#addimages").on("change", function(e) {
      $(".addfileloctn_prod_images").empty();
      addprod_images = [];
      var files = e.target.files;
      for (var i = 0; i < files.length; i++) {
        var file = files[i];
        addprod_images.push(file);
        add_img_Files(file, i);
      }
    });
  } //add image ends here

  $.ajax({
    url: listemployeesrole_api,
    type: 'get',
    headers: {
      "content-type": 'application/json',
      "Authorization": "Token " + localStorage.token
    },
    success: function(data) {
      for (var i = 0; i < data.category.length; i++) {
        $('#addcatgy').append('<option value=' + data.category[i].id + '>' + data.category[i].name + '</option>');
      }
    },

    error: function(data) {
      console.log("error occured during category load");
    }
  });

})
//image file on load
//ext local files append fn
function add_img_Files(f, i) {
  var reader = new FileReader();
  reader.onload = (function(file) {
    return function(e) {
      $(".uploadprodimage").append("<div class='col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12 mt10 plr5 addimage" + i + "'> <div class='itemsContainer'> <div class='image image_mt10'> <img src='" + e.target.result + "' class='img-responsive uploadimg' title='" + file.name + "' style='height: 100px;width: 100px;'/> </div><div class='play'> <a onclick='deletethisimage_local(" + i + ",\"" + file.name + "\")' class='bt_pointer'><img src='../img/deletebtn.svg'/></a> </div></div></div>");

    };
  })(f, i);
  reader.readAsDataURL(f);
}
//delete image starts hre
//delete images locally fn starts here
function deletethisimage_local(i, fileName) {
  $('.addimage' + i).remove();
  for (var i = 0; i < addprod_images.length; i++) {
    if (addprod_images[i].name === fileName) {
      addprod_images.splice(i, 1);
      break;
    }
  }
}

function submitaddorders() { //submit functionality starts here
  var postData = new FormData();

  if ($('#addcatgy').val() == 0) {
    $("#snackbarerror").text("Mention the category");
    showiperrtoast();
    event.stopPropagation();
    return;
  } else {
    postData.append("category", $('#addcatgy').val());
  }
  if ($('#productname').val() == 0) {
    $("#snackbarerror").text("Product name is required");
    showiperrtoast();
    event.stopPropagation();
    return;
  } else {
    postData.append("product", $('#productname').val());

  }
  if ($('#pickupname').val() == '') {
    $("#snackbarerror").text("Pickup name is required");
    showiperrtoast();
    event.stopPropagation();
    return;
  } else {
    postData.append("pickup_name", $('#pickupname').val());

  }
  if ($('#deliveryname').val() == '') {
    $("#snackbarerror").text("Delivery name is required");
    showiperrtoast();
    event.stopPropagation();
    return;
  } else {
    postData.append("del_name", $('#deliveryname').val());

  }

  if ($('#pickuppn').val() == 0) {
    $("#snackbarerror").text("Pickup phoneno. is required");
    showiperrtoast();
    event.stopPropagation();
    return;
  } else {
    postData.append("pickup_mob", $('#pickuppn').val());

  }

  if ($('#deliverypn').val() == 0) {
    $("#snackbarerror").text("Delivery phoneno. is required");
    showiperrtoast();
    event.stopPropagation();
    return;
  } else {
    postData.append("del_mob", $('#deliverypn').val());

  }
  if ($('#pickupaddr').val() == '') {
    $("#snackbarerror").text("Pickup address is required");
    showiperrtoast();
    event.stopPropagation();
    return;
  } else {
    postData.append("pickup_loc", $('#pickupaddr').val());

  }
  if ($('#dropaddr').val() == '') {
    $("#snackbarerror").text("Delivery address is required");
    showiperrtoast();
    event.stopPropagation();
    return;
  } else {
    postData.append("delivery_loc", $('#dropaddr').val());

  }
  if ($('#pickuptype').val() == 0) {
    $("#snackbarerror").text("Pickup type is required");
    showiperrtoast();
    event.stopPropagation();
    return;
  } else {
    postData.append("pickup_type", $('#pickuptype').val());

  }
  if ($('#deliverytype').val() == 0) {
    $("#snackbarerror").text("Delivery type is required");
    showiperrtoast();
    event.stopPropagation();
    return;
  } else {
    postData.append("delivery_type", $('#deliverytype').val());

  }
  if ($('#weightrange').val() == 0) {
    $("#snackbarerror").text("Weight range is required");
    showiperrtoast();
    event.stopPropagation();
    return;
  } else {
    postData.append("product_weight", $('#weightrange').val());

  }
  if (addprod_images.length != 0) {
    for (i = 0; i < addprod_images.length; i++) {
      postData.append("image", addprod_images[i]);
    }
  }
  if ($("#time").val() != "") {
    postData.append("pickup_time", $('#date_picker1').val() + 'T' + $('#time').val() + ':00');
  }

  if ($('#descriptfadd').val() != '') {
    postData.append("description", $('#descriptfadd').val());
  }

  console.log(postData);

  $(".submitbtnord").html('submit <img src="../img/loader.gif">');
  $(".submitbtnord").attr("disabled", true);

  $.ajax({
    url: addorder_api,
    type: 'post',
    data: postData,
    crossDomain: true,
    contentType: false,
    processData: false,
    headers: {
      "Authorization": "Token " + localStorage.token
    },
    success: function(data) {

      $(".submitbtnord").html('submit');
      $(".submitbtnord").attr("disabled", false);
      $("#snackbarsuccs").text("Order placed");
      showsuccesstoast();
      window.location.href = "post-orders.html";

    },
    error: function(data) {
      $(".submitbtnord").html('submit');
      $(".submitbtnord").attr("disabled", false);
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  })
} //submit functionality ends here

$(function() {
  loadunconformed_orders(0);
});

//load funcion starts here
function loadunconformed_orders(type) {

  var postData = JSON.stringify({
    "status": 1,
    "date" : $("#date_picker").val()
  });

  if (type == 0) {
    var url = listorder_api;
  } else {
    var url = type;
  }

  $.ajax({
    url: url,
    data: postData,
    type: "POST",
    headers: {
      "content-type": "application/JSON",
      "Authorization": "token " + localStorage.token
    }
  }).done(function(data) {
    $(".findalldr").hide();
    $(".gobtnall").attr("disabled",false);
    $(".unconformedlist,.imgforunconformedodrs").empty();
    if (data.results.length != 0) {
      $(".unconformedclass").show();
      for (var i = 0; i < data.results.length; i++) {
        var image;
        (data.results[i].pickup_image == null || data.results[i].pickup_image.length == 0) ? image = '../img/noimage.png': image = data.results[i].pickup_image[0].image;

        var created_on = data.results[i].created_on;
        var newdate = created_on.split('T');
        var now = newdate[0].split('-');
        var timeval = Convert_time(newdate[1].slice(0, 8));
        var datenew = now[2] + '/' + now[1] + '/' + now[0];
        $(".unconformedlist").append(`<tr><td>${i + 1}</td><td>  ${data.results[i].id}</td><td onclick="viewproduct(this,${data.results[i].id},0)"><center><img src="${image}" class="img-responsive tab_img" href="#viewfullimg" data-toggle="modal"></center></td><td>${data.results[i].product}</td><td>${data.results[i].pickup_name}</td><td>${datenew} ${timeval}</td><td><img src="../img/buy.svg" class="width50"></td><td>  <a data-toggle="modal" onclick="loadaddress('${data.results[i].id}','${data.results[i].pickup_type}');"  href="#viewmoremodalplaced" class="asntsk">More</a></td><td><a onclick="getuserid(${data.results[i].id})" class="asntsk">click here</a></td></tr>`);
      }

      if (data.next_url != null) {
        $("#urlloader").show();
        var next1 = (data.next_url).split('=');
        var val = data.count / data.page_size;
        if (val % 1 === 0) {
          //if number is integer
          var val = val;
        } else {
          var val = parseInt(val) + 1;
        }
        var obj = $('#pagination').twbsPagination({
          totalPages: val,
          visiblePages: 5,
          onPageClick: function(event, page) {
            console.info(page);
            loadunconformed_orders(next1[0] + "=" + (page));
          }
        });
      }
    } else {
      $(".unconformedclass").hide();
      $('.imgforunconformedodrs').empty().append(`<center><img id="theImg" src="../img/ndf.png" height="250px" width="250px"/>`);
    }
  }).fail(function(data) {
    $(".findalldr").hide();
    $(".gobtnall").attr("disabled",false);
    for (var key in JSON.parse(data.responseText)) {
      $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
    }
    showerrtoast();
  })
}

//get id func starts here
function getuserid(id) {
user_id = id;
$("#asngtsk_bd").modal();
}

//send amount conformed
function sendamt() {

if($("#estcost").val() == ""){
  $("#snackbarerror").text("Enter the cost to submit");
  showerrtoast();
  event.stopPropagation();
  return;
}


  $(".addcostldr").show();
  $(".addcostbtn").attr("disabled",true);
  var postData = JSON.stringify({
    	"product_cost":$("#estcost").val()
  })
  $.ajax({
    url: listproductimg_api+ user_id + "/product_cost/",
    data: postData,
    type: "POST",
    headers: {
      "content-type": "application/JSON",
      "Authorization": "token " + localStorage.token
    }
  }).done(function(data) {
    $(".closeadmodl").click();
    $(".addcostldr").hide();
    $(".addcostbtn").attr("disabled",false);
    $('#urlloader').empty();
    $('#urlloader').append(`<ul class="list-inline pagination floatright" id="pagination"></ul>`);
    loadunconformed_orders(0);
  }).fail(function(data){
    $(".addcostldr").hide();
    $(".addcostbtn").attr("disabled",false);
    for (var key in JSON.parse(data.responseText)) {
      $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
    }
    showerrtoast();
  })
}

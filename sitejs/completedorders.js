//list completed orders starts here
function listcompletedorders(type) {

  if (type == 0) {
    var url = listorder_api;
  } else {
    var url = type;
  }

  $(".completeddynload").empty();
  var postData = JSON.stringify({
    "status": 4,
    "date": $("#date_picker").val()
  });
  $.ajax({
    url: url,
    type: 'POST',
    data: postData,
    headers: {
      "content-type": 'application/json',
      "Authorization": "Token " + localStorage.token
    },
    success: function(data) {
      $(".findalldr").hide();
      $(".gobtnall").attr("disabled",false);
      if (data.results.length == 0) {
        $(".completedclass").hide();
        $(".completeddiv").empty().prepend('<center><img id="theImg" src="../img/ndf.png" height="400px" width="400px"/>');


      } else {
        $(".completedclass").show();

        for (var i = 0; i < data.results.length; i++) {
          if (data.results[i].image == null) {
            var image = '../img/noimage.png';
          } else {
            var image = data[i].image;
          }

          var created_on = data.results[i].created_on;
          var newdate = created_on.split('T');
          var now = newdate[0].split('-');
          var timeval = Convert_time(newdate[1].slice(0, 8));
          var datenew = now[2] + '/' + now[1] + '/' + now[0];

          var picktype = (data.results[i].pickup_type) ? '../img/pick.svg' : '../img/buy.svg';


          $(".completeddynload").append(`<tr><td>${(i + 1)}</td><td>${data.results[i].id}</td><td onclick="viewproduct(this,${data.results[i].id},0)"><center><img src="${image}" class="img-responsive tab_img" href="#viewfullimg" data-toggle="modal"></center></td><td>${data.results[i].product}</td><td>${data.results[i].pickup_name}</td><td>${datenew} ${timeval}</td><td><img src=${picktype} class="width50"></td><td onclick="viewproduct(this,${data.results[i].id},0)"><center><img src="${image}" class="img-responsive tab_img" href="#viewfullimg" data-toggle="modal"></center></td><td onclick="viewproduct(this,${data.results[i].id},1)"><center><img src="${image}" class="img-responsive tab_img" href="#viewfullimgdelvery" data-toggle="modal"></center></td><td><a data-toggle="modal" href="#modal_ordercmp" onclick="appendlocationcomp(${data.results[i].id});" class="asntsk">More</a></td></tr>`);
        }
      }
      if (data.next_url != null) {
        $("#urlloader").show();

        var next1 = (data.next_url).split('=');
        var val = data.count / data.page_size;
        if (val % 1 === 0) {
          //if number is integer
          var val = val;
        } else {
          var val = parseInt(val) + 1;
        }
        var obj = $('#pagination').twbsPagination({
          totalPages: val,
          visiblePages: 5,
          onPageClick: function(event, page) {
            console.info(page);
            listcompletedorders(next1[0] + "=" + (page));
          }
        });
      }
    },
    error: function(data) {
      $(".findalldr").hide();
      $(".gobtnall").attr("disabled",false);
      // $(".completedclass").hide();
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  });
}

//append data
function appendlocationcomp(me) {
  localStorage.userid = me

  $.ajax({
    url: singleorder_api + me + '/',
    type: 'get',
    headers: {
      "content-type": 'application/json',
      "Authorization": "Token " + localStorage.token
    },
    success: function(data) {
      $('.corner , .corner1,.corner2').css("border-bottom", "4px solid red");
      $('.completedcost').text(" " +data.orderdetails.delivery_cost);
      $('.completetype').text(" " +data.payment_type);
      $('#appendpickupdetcomp .pickernamecomp').text(" " +data.pickup_name);
      $('#appendpickupdetcomp .pickerph_nocomp').text(" " +data.pickup_mob);
      $('#appendpickupdetcomp .pickerloccomp').text(" " +data.pickup_loc);
      $('#appenddeldetcomp .deliverynamecomp').text(" " +data.del_name);
      $('#appenddeldetcomp .deliveryph_nocomp').text(" " +data.del_mob);
      $('#appenddeldetcomp .deliveryloccomp').text(" " +data.del_loc);
      $('#appendtimecompld .distcomp').text(" " +data.orderdetails.approx_dist + " Kms");
      $('#appendtimecompld .estimatecomp').text(time_is);
      var cost = (data.orderdetails.product_cost == null) ? 0 : data.orderdetails.product_cost;
      $('#appendtimecompld .paymtdetcomp').text( " Rs." + (parseFloat(data.orderdetails.delivery_cost) + parseFloat(cost)));
      $('.deliveryboynamecomp').text(" " +data.orderassignment.assign_employee.first_name);
      $('.deliveryboyph_nocomp').text(" " +data.orderassignment.assign_employee.username);
      $('.editclassemp').attr('src', data.orderassignment.assign_employee.profile_pic);
      var time_taken = (data.orderdetails.time_taken).split(":");
      if (time_taken[0] != "00") {
        var time_is = data.orderdetails.time_taken;
        $('.timeexp').text(time_is + " Hours");
      } else if (time_taken[1] != "00") {
        var time_is = data.orderdetails.time_taken.slice(3);
        $('.timeexp').text(time_is + " Mins");
      } else {
        $('.timeexp').text('0 Min');
      }
    },error:function(data){
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  });
}

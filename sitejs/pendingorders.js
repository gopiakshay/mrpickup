//pending orders list dynamic
function listpendingorders(type) {

  if (type == 0) {
    var url = listorder_api;
  } else {
    var url = type;
  }

  var postData = JSON.stringify ({
    "status": 3,
    "date": $("#date_picker").val()
  });

  $.ajax({
    url: url,
    type: 'POST',
    data: postData,
    headers: {
      "content-type": 'application/json',
      "Authorization": "Token " + localStorage.token
    },
    success: function(data) {
      $('.listpendingorders,.pendemptyimgappend').empty();
      $(".findalldr").hide();
      $(".gobtnall").attr("disabled",false);
      if (data.results.length == 0) {
        $(".pendingclass").hide();
        $(".pendemptyimgappend").empty().append('<center><img id="theImg" src="../img/ndf.png" height="250px" width="250px"/>');
      } else {

        $(".pendingclass").show();
        for (var i = 0; i < data.results.length; i++) {

          if (data.results[i].pickup_image == null || data.results[i].pickup_image.length == 0) {
            var image = '../img/noimage.png';
          } else {
            var image = data.results[i].pickup_image[0].image;
          }

          var created_on = data.results[i].created_on;
          var newdate = created_on.split('T');
          var now = newdate[0].split('-');
          var timeval = Convert_time(newdate[1].slice(0, 8));
          var datenew = now[2] + '/' + now[1] + '/' + now[0];

          var picktype = (data.results[i].pickup_type) ? '../img/pick.svg' : '../img/buy.svg';

          $('.listpendingorders').append(`<tr><td>${i + 1}</td><td>${data.results[i].id}</td><td onclick="viewproduct(this,${data.results[i].id},0)"><center><img src="${image}" class="img-responsive tab_img" href="#viewfullimg" data-toggle="modal"></center></td><td>${data.results[i].product}</td><td>${data.results[i].pickup_name}</td><td class="whitenowrap">${datenew} ${timeval}</td><td><img src=${picktype} class="width50"></td><td onclick="viewproduct(this,${data.results[i].id},0)"><center><img src="${image}" class="img-responsive tab_img" href="#viewfullimg" data-toggle="modal"><td><a onclick="initMap(${data.results[i].pickup_lat},${data.results[i].pickup_long})" data-toggle="modal" href="#trackingmapmodal" class="btn btn-circle btn-icon-only fsearchrbtn"><i class="icon-info icnsend"></i></a></td><td>${data.results[i].status.name}</td><td><a data-toggle="modal" onclick="morefunction(${data.results[i].id})" href="#deliverymodal" class="asntsk">More</a></td></tr>`);
        }
      }

      if (data.next_url != null) {
        $("#urlloader").show();

        var next1 = (data.next_url).split('=');
        var val = data.count / data.page_size;
        if (val % 1 === 0) {
          //if number is integer
          var val = val;
        } else {
          var val = parseInt(val) + 1;
        }
        var obj = $('#pagination').twbsPagination({
          totalPages: val,
          visiblePages: 5,
          onPageClick: function(event, page) {
            console.info(page);
            listpendingorders(next1[0] + "=" + (page));
          }
        });
      }
    },

    error: function(data) {
      $(".findalldr").hide();
      $(".gobtnall").attr("disabled",false);
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  });
}

//more function for pending
function morefunction(me) {

  localStorage.userid = me;
  $.ajax({
    url: singleorder_api + me + '/',
    type: 'get',
    headers: {
      "content-type": 'application/json',
      "Authorization": "Token " + localStorage.token
    },
    success: function(data) {
      console.log(data.pickup_type)
      if (data.pickup_type) {

        $('#paymdet').hide();
        $('.corner').css("border-bottom", "4px solid red");
        $('.corner1').css("border-bottom", "");
        $('#compdivload .idorderpend').text(" " +data.id);
        $('#compdivload .paymentpenddet').text(" " +data.payment_type);
        $('#appendpickupdet .pickername').text(" " +data.pickup_name);
        if (data.pickup_mob == "") {
          $('#appendpickupdet .pickerph_no').text(' Not Mentioned');
        } else {
          $('#appendpickupdet .pickerph_no').text(" " +data.pickup_mob);
        }
        if (data.pickup_loc == "") {
          $('#appendpickupdet .pickerloc').text(' Not Mentioned');
        } else {
          $('#appendpickupdet .pickerloc').text(" " +data.pickup_loc);
        }
        //location of picker to be given
        var picktype = (data.pickup_type) ? 'Pick & Deliver' : 'Buy & Deliver';
        $('.payment_type').text(" " +picktype);
        $('.delcost').text(" " +parseFloat(data.orderdetails.delivery_cost));
        $('#appenddeldet .deliveryname').text(" " +data.del_name);
        $('#appenddeldet .deliveryph_no').text(" " +data.del_mob);
        $('#appenddeldet .deliveryloc').text(" " +data.del_loc);
        $('#approdist .dist').text(" " +(data.orderdetails.approx_dist).toFixed(2) + " Kms");
        $('#estimatetime .estimate').text(time_is);
        $('#costval .costvalue').text(" " +parseFloat(data.orderdetails.delivery_cost));
        $('.emp_image').attr('src', data.orderassignment.assign_employee.profile_pic);
        $('.dlvboyname').text(" " +data.orderassignment.assign_employee.first_name);
        $('.deliverboyyph_no').text(" " +data.orderassignment.assign_employee.username);
      } else {

        $('#paymdet').show();
        $('.corner').css("border-bottom", "4px solid red");
        $('.corner1').css("border-bottom", "");
        $('#compdivload .idorderpend').text(" " +data.id);
        var picktype = (data.pickup_type) ? 'Pick & Deliver' : 'Buy & Deliver';
        $('.payment_type').text(" " +picktype);
        $('.delcost').text(" " +(parseFloat(data.orderdetails.delivery_cost) + parseFloat(data.orderdetails.product_cost).toFixed(2)));

        $('#compdivload .paymentpenddet').text(" " +data.payment_type);
        $('#appendpickupdet .pickername').text(" " +data.pickup_name);
        $('#appendpickupdet .pickerph_no').text(" " +data.pickup_mob);
        $('#appendpickupdet .pickerloc').text(" " +data.pickup_loc);
        $('#appenddeldet .deliveryname').text(" " +data.del_name);
        $('#appenddeldet .deliveryph_no').text(" " +data.del_mob);
        $('#appenddeldet .deliveryloc').text(" " +data.del_loc);
        $('#approdist .dist').text(" " +(data.orderdetails.approx_dist).toFixed(2) + " Kms");
        $('#estimatetime .estimate').text(time_is);
        $('#costval .costvalue').text(parseFloat(" " +data.orderdetails.delivery_cost));
        $('#paymdet .paymtdet').text(parseFloat(" " +data.orderdetails.product_cost));
        $('.emp_image').attr('src', data.orderassignment.assign_employee.profile_pic);
        $('.dlvboyname').text(" " +data.orderassignment.assign_employee.first_name);
        $('.deliverboyyph_no').text(" " +data.orderassignment.assign_employee.username);
      }
      var time_taken = (data.orderdetails.time_taken).split(":");
      if (time_taken[0] != "00") {
        var time_is = data.orderdetails.time_taken;
        $('.timeexp').text(time_is + " Hours");
      } else if (time_taken[1] != "00") {
        var time_is = data.orderdetails.time_taken.slice(3);
        $('.timeexp').text(time_is + " Mins");
      } else {
        $('.timeexp').text('0 Min');
      }
    },
    error: function() {
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  });
}

function myMap(lat, long) {
  // var myCenter = new google.maps.LatLng(51.508742, -0.120850);
  var myCenter = new google.maps.LatLng(lat,long);

  var mapCanvas = document.getElementById("map");
  var mapOptions = {
    center: myCenter,
    zoom: 5
  };
  var map = new google.maps.Map(mapCanvas, mapOptions);
  var marker = new google.maps.Marker({
    position: myCenter
  });
  marker.setMap(map);
}


function initMap(lati, longi) {
  // load_mapon();
  var myLatlng = {
    lat: lati,
    lng: longi
  };

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 4,
    center: myLatlng
  });

  var marker = new google.maps.Marker({
    position: myLatlng,
    map: map,
    title: 'Click to zoom'
  });

  map.addListener('center_changed', function() {
    // 3 seconds after the center of the map has changed, pan back to the
    // marker.
    window.setTimeout(function() {
      map.panTo(marker.getPosition());
    }, 3000);
  });

  marker.addListener('click', function() {
    map.setZoom(8);
    map.setCenter(marker.getPosition());
  });

  // google.maps.event.addDomListener(window, "load", myMap);
}


function load_mapon() {
  var head = document.getElementsByTagName('head')[0];
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyApphwNBRGGPclB1lopdI_CCPedOc3pneE&callback=myMap';
  head.appendChild(script);
}

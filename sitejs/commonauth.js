var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
$(function() {
  $(document).ajaxError(function(event, jqxhr, settings, thrownError){
    (jqxhr.status == 401) ? window.location.href = "../index.html" : "";
  });
  $(window).load(function() {
    $('#preloader').fadeOut('slow', function() {
      $(this).remove();
    });
  });

  $("body").append('<div id="snackbarsuccs"></div><div id="snackbarerror"></div>');
});



(localStorage.used_id != 1) ?  ($("li.nav-item").eq(1).hide() && $("li.nav-item").eq(5).hide()) : "";


//success toast fn starts here
function showsuccesstoast() {
  var x = document.getElementById("snackbarsuccs");
  x.className = "show";
  setTimeout(function() {
    x.className = x.className.replace("show", "");
  }, 5000);
}

//failure toast fn starts here
function showerrtoast() {
  var x = document.getElementById("snackbarerror");
  x.className = "show";
  setTimeout(function() {
    x.className = x.className.replace("show", "");
  }, 5000);
}

//failure toast fn starts here
function showiperrtoast() {
  var x = document.getElementById("snackbarerror");
  x.className = "show";
  setTimeout(function() {
    x.className = x.className.replace("show", "");
  }, 3000);
}


//function to show img
function productcom(me) {
  var url = $($(me).html()).find('img').attr('src');
  $('.productview1').empty().append("<br><img src='" + url + "' height='300px' width='300px'><br><br>");
}
//image load for product and employee
function productimg(me) {
  var url = $($(me).html()).find('img').attr('src');
  $('.productview1').empty().append("<br><img src='" + url + "' height='300px' width='300px'><br><br>");
}

function employeeimg(me) {
  var url = $($(me).html()).find('img').attr('src');
  $('.employeeprofilee').empty().append("<br><img src='" + url + "' height='300px' width='300px'><br><br>");
}

function viewproduct(me, id, type) {
  var url = $($(me).html()).find('img').attr('src');
  if (url == '../img/noimage.png') {
    $('#appendproductimg').empty().append("<center><br><img src='" + url + "' class='img-responsive h100 mb10'><br><br></center>");
    $('#appendproductimgdelivery').empty().append("<center><br><img src='" + url + "' class='img-responsive h100 mb10'><br><br></center>");

  } else {

    $.ajax({
      url: listproductimg_api + id + '/list_images/',
      type: 'get',
      headers: {
        "content-type": 'application/json',
        "Authorization": "Token " + localStorage.token
      },
      success: function(data) {
        $('#appendproductimg').empty();
        if (type == 0) { //if starts here
          if (data.pickup_image.length == 1) {
            $('#appendproductimg').append(`<center><br><img src='${data.pickup_image[0].image}' class='img-responsive h100 mb10'><br><br></center>`)

          } else {
            for (var i = 0; i < data.pickup_image.length; i++) {
              $('#appendproductimg').append(`<div class="col-md-6"><img src='${data.pickup_image[i].image}' class='img-responsive h100 mb10'></div>`)
            }
          }
        } //if for type ends here
        else {
          if (data.delivery_image.length == 1) {
            $('#appendproductimgdelivery').append(`<center><br><img src='${data.delivery_image[0].image}' class='img-responsive h100 mb10'><br><br></center>`)

          } else {
            for (var i = 0; i < data.delivery_image.length; i++) {
              $('#appendproductimgdelivery').append(`<div class="col-md-6"><img src='${data.delivery_image[i].image}' class='img-responsive h100 mb10'></div>`)
            }
          }
        } //else for type ends here
        // }

      },
      error: function(data) {

      }
    });
  } //else ends here
}

//change password modal srch_inpt// change password starts here
$('.currentpasswrd').focusout(function() {
  if ($('.currentpasswrd').val() == "") {
    $("#snackbarerror").text("Current password is required");
    showerrtoast();
    event.stopPropagation();
    return;

  }
});
$('.newpasswrd').focusout(function() {
  if ($('.newpasswrd').val() == "") {
    $("#snackbarerror").text("New password is required");
    showerrtoast();
    event.stopPropagation();
    return;

  }
});
$('.renewpasswrd').focusout(function() {
  if ($('.renewpasswrd').val() == "") {
    $("#snackbarerror").text("Re-enter new password is required");
    showerrtoast();
    event.stopPropagation();
    return;

  }
});

function changepassword() {
  if ($('.currentpasswrd').val() == "") {
    $("#snackbarerror").text("Current password is required");
    showerrtoast();
    event.stopPropagation();
    return;

  } else if ($('.newpasswrd').val() == "") {
    $("#snackbarerror").text("New password is required");
    showerrtoast();
    event.stopPropagation();
    return;

  } else if ($('.renewpasswrd').val() == "") {
    $("#snackbarerror").text("Re-enter new password is required");
    showerrtoast();
    event.stopPropagation();
    return;

  } else if ($('.newpasswrd').val() != $('.renewpasswrd').val()) {
    $("#snackbarerror").text("New password and re-enter new password must be same");
    showerrtoast();
    event.stopPropagation();
    return;

  } else if ($('.currentpasswrd').val() == $('.renewpasswrd').val()) {
    $("#snackbarerror").text("Current password and re-enter new password must be same");
    showerrtoast();
    event.stopPropagation();
    return;

  } else {
    changepasswordfunc();
  }
}

function changepasswordfunc() {

  $(".changepassBtn").html('Change <img src="../img/loader.gif">');
  $(".changepassBtn").attr("disabled", true);

  var postdata = JSON.stringify({
    "old_password": $(".currentpasswrd").val(),
    "new_password": $(".newpasswrd").val()
  });

  $.ajax({
    url: changepasswordforadmin_api,
    type: 'post',
    data: postdata,
    headers: {
      "content-type": 'application/json'
    },
    beforeSend: function(xhr) {
      xhr.setRequestHeader("Authorization", "Token " + localStorage.token)
    },
    success: function(data) {

      $(".changepassBtn").html('Change');
      $(".changepassBtn").attr("disabled", false);
      $(".closechangepass").click();

    },
    error: function(data) {
      $(".changepassBtn").html('Change');
      $(".changepassBtn").attr("disabled", false);

      var errtext = "";
      for (var key in JSON.parse(data.responseText)) {
        errtext = JSON.parse(data.responseText)[key];
      }
      $("#snackbarerror").text(errtext.detail);
      showerrtoast();

    }
  }).done(function(dataJson) {
    $(".currentpasswrd,.newpasswrd,.renewpasswrd").val("");
    $("#snackbarsuccs").text("Password Changed Sucessfully!");
    showsuccesstoast();
    // var pageURL = $(location).attr("href");
    // window.location.href = pageURL;
  });

} //change password fn ends here

//search function start here
function searchbtnfun(type) {
  $('#urlloader').empty();
  $('#urlloader').append(`<ul class="list-inline pagination floatright" id="pagination"></ul>`);
  if (type == 0) {
    if ($('.datepickerorder').val() == "") {
      $("#snackbarerror").text("Enter some date to search");
      showerrtoast();
      event.stopPropagation();
    }
  } else {
    $(".findalldr").show();
    $(".gobtnall").attr("disabled", true);
    $('.datepickerorder').val("");
  }
  var value = $("li.tab_clr.active").find("a").data("attr_val");
  switch (value) {
    case 0:
      loadunconformed_orders(0);
      break;
    case 1:
      listorders(0);
      break;
    case 2:
      listpendingorders(0);
      break;
    case 3:
      listcompletedorders(0);
      break;
    case 4:
      listcancelledorders(0);
      break;
    case 5:
      requestcancelled(0);
      break;
  }

}


//logout function starts here
//logout fn starts here
function logout() {

  $.ajax({
    url: logout_api,
    type: 'post',
    headers: {
      "content-type": 'application/json'
    },
    beforeSend: function(xhr) {
      xhr.setRequestHeader("Authorization", "Token " + localStorage.token)
    },
    success: function(data) {
      sessionStorage.clear();
      localStorage.clear();

    },
    error: function(data) {
      sessionStorage.clear();
      localStorage.clear();
      window.location.replace("../index.html");
    }
  }).done(function(dataJson) {
    window.location.replace("../index.html");
  });
} //logout fn starts here

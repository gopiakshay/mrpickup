//function to load data starts here
function listorders(type) {

  if (type == 0) {
    var url = listorder_api;
  } else {
    var url = type;
  }

  var postData = JSON.stringify({
    "status": 2,
    "date": $("#date_picker").val()
  });
  $.ajax({
    url: url,
    type: 'POST',
    data: postData,
    headers: {
      "content-type": 'application/json',
      "Authorization": "Token " + localStorage.token
    },
    success: function(data) {
      $(".placedoredrsimg,.addplacedorders").empty();
      $(".findalldr").hide();
      $(".gobtnall").attr("disabled", false);
      if (data.results.length == 0) {
        $(".placedclass").hide();
        $(".placedoredrsimg").empty().append('<center><img id="theImg" src="../img/ndf.png" height="250px" width="250px"/>');
      } else {
        $(".placedclass").show();
        for (var i = 0; i < data.results.length; i++) {
          if (data.results[i].pickup_image == null || data.results[i].pickup_image.length == 0) {
            var image = '../img/noimage.png';
          } else {
            var image = data.results[i].pickup_image[0].image;
          }
          var created_on = data.results[i].created_on;
          var newdate = created_on.split('T');
          var now = newdate[0].split('-');
          var timeval = Convert_time(newdate[1].slice(0, 8));
          var datenew = now[2] + '/' + now[1] + '/' + now[0];
            var product;
            (data.results[i].pickup_type) ? product = '../img/pick.svg' : product = '../img/buy.svg';

            $(".addplacedorders").append(`<tr><td>${i + 1}</td><td>  ${data.results[i].id}</td><td onclick="viewproduct(this,${data.results[i].id},0)"><center><img src="${image}" class="img-responsive tab_img" href="#viewfullimg" data-toggle="modal"></center></td><td>${data.results[i].product}</td><td>${data.results[i].pickup_name}</td><td>${datenew} ${timeval}</td><td><img src=${product} class="width50"></td><td>  <a data-toggle="modal" onclick="loadaddress('${data.results[i].id}','${data.results[i].pickup_type}');"  href="#viewmoremodalplaced" class="asntsk">More</a></td><td><a data-toggle="modal" href="#asngtsk" onclick="listnearestemployee(${data.results[i].id},0)" class="asntsk">click here</a></td></tr>`);
          }
      }

      if (data.next_url != null) {
        $("#urlloader").show();

        var next1 = (data.next_url).split('=');
        var val = data.count / data.page_size;
        if (val % 1 === 0) {
          //if number is integer
          var val = val;
        } else {
          var val = parseInt(val) + 1;
        }
        var obj = $('#pagination').twbsPagination({
          totalPages: val,
          visiblePages: 5,
          onPageClick: function(event, page) {
            console.info(page);
            listorders(next1[0] + "=" + (page));
          }
        });
      }
    },
    error: function(data) {
      $(".findalldr").hide();
      $(".gobtnall").attr("disabled", false);
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  });
}

//list the nearest employee on click assign
function listnearestemployee(datam,type_val) {
  $('.listemployee').empty();
  placerdenied = type_val;
  localStorage.empid = datam;
  $.ajax({
    url: listemployeenearest_api + datam + '/',
    type: 'get',
    headers: {
      "content-type": 'application/json',
      "Authorization": "Token " + localStorage.token
    },
    success: function(data) {

      $('.listemployeee').empty();
      if(data.length != 0){
        $(".assignempthead").show();
      for (var i = 0; i < data.length; i++) {
        $('.listemployeee').append(`<tr><td> ${data[i].first_name +" " +data[i].last_name }</td><td> ${data[i].id} </td><td> ${data[i].username} </td><td>${data[i].distance} Kms</td><td><a onclick='clickassignanother(${data[i].id}, ${localStorage.empid })' class='asntsk empaddBtn${data[i].id}'>Assign<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr editofferldr${data[i].id}" style="display: none;"></i></a></td></tr>`);
      }
    }else{
      $(".assignempthead").hide();
      $('.listemployeee').append(`<center><strong>No Employees To List</center></strong>`);
    }
    },
    error: function(data) {

      $(".subadmintable").hide();
      $(".tablestart").text('No Data Found').css('color', '#e43a45')
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  });
}


//assign employee on assign function
function clickassignanother(data, id) {
  $(".empaddBtn"+id).show();
  $(".empaddBtn"+id).attr("disabled", false);

  var postData = JSON.stringify({
    "order": id,
    "assign_employee": data
  });

  $.ajax({
    url: assignemployee_api,
    type: 'post',
    data: postData,
    headers: {
      "content-type": 'application/json',
      "Authorization": "Token " + localStorage.token
    },
    success: function(data) {

      $(".empaddBtn"+id).hide();
      $(".empaddBtn"+id).attr("disabled", false);
      $(".closeadd_empmodal").click();
      $("#snackbarsuccs").text("Employee assigned Successfully!");
      showsuccesstoast();
      (placerdenied == 0) ? listorders(0) : requestcancelled();
    },
    error: function(data) {

      $(".empaddBtn"+id).hide();
      $(".empaddBtn"+id).attr("disabled", false);

      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  })

}
//ends here

//function to convert time
function Convert_time(time) {
  time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
  if (time.length > 1) {
    time = time.slice(1);
    time[3] = +time[0] < 12 ? 'AM' : 'PM';
    time[0] = +time[0] % 12 || 12;
  }
  return time.join('');
}

//function to append more details

function loadaddress(id, picktype) {
  $.ajax({
    url: singleorder_api + id + '/',
    type: 'get',
    headers: {
      "content-type": 'application/json',
      "Authorization": "Token " + localStorage.token
    },
    success: function(data) {
      //       {
      //     "id": 3,
      //     "pickup_name": "Jegatheeswaran",
      //     "pickup_loc": "477, Model House Rd, CIT Nagar West, CIT Nagar, Chennai, Tamil Nadu 600035, India",
      //     "pickup_mob": "9715868154",
      //     "del_name": "Krishna",
      //     "del_loc": "Nivedha Apartments, West Rd, CIT Nagar West, West Saidapet, Chennai, Tamil Nadu 600035, India",
      //     "del_mob": "9715868154",
      //     "pickup_type": true,
      //     "pickup_time": "2017-07-01T12:33:55",
      //     "created_on": "2017-11-23T16:22:09.447137",
      //     "orderdetails": {
      //         "id": 3,
      //         "approx_dist": 1.635,
      //         "time_taken": "00:07:05",
      //         "delivery_cost": 80,
      //         "product_cost": null,
      //         "waiting_time": null,
      //         "waiting_cost": null,
      //         "status": {
      //             "id": 1,
      //             "name": "Order Placed"
      //         },
      //         "delivery_time": null
      //     },
      //     "orderassignment": null
      // }
      var pickty = (picktype == 'true') ? 'Pick & Deliver' : 'Buy & Deliver';
      $('.delcharge').text(" "+ data.orderdetails.delivery_cost);
      $('.paytype').text(" "+ data.payment_type);
      $('.deliverytype').text(" "+ data.delivery_type);
      $('.picktype').text("  " + pickty);
      $('.pickupname').text(" "+ data.pickup_name);
      $('.pickuploc').text(" "+ data.pickup_loc);
      $('.pickuphno').text(" "+ data.pickup_mob);
      $('.deliveryname').text(" "+ data.del_name);
      $('.deliveryaddr').text(" "+ data.del_loc);
      $('.deliverynum').text(" "+ data.del_mob);

      var time_taken = (data.orderdetails.time_taken).split(":");
      if (time_taken[0] != "00") {
        var time_is = data.orderdetails.time_taken;
        $('.timeexp').text(time_is + " Hours");
      } else if (time_taken[1] != "00") {
        var time_is = data.orderdetails.time_taken.slice(3);
        $('.timeexp').text(time_is + " Mins");
      } else {
        $('.timeexp').text('0 Min');
      }

    },
    error: function(data) {
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  });
}
